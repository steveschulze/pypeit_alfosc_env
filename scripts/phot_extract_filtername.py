#!/usr/bin/env python3
import sys, os
import astropy.nddata as nddata
import astropy.io.fits as fits


def get_filter_name(hdr):
    if 'ALFLTNM' in hdr and hdr['ALFLTNM'] != 'Open':
        return hdr['ALFLTNM']
    if 'FAFLTNM' in hdr and hdr['FAFLTNM'] != 'Open':
        return hdr['FAFLTNM']
    if 'FBFLTNM' in hdr and hdr['FBFLTNM'] != 'Open':
        return hdr['FBFLTNM']
    return ''



if __name__ == '__main__':
    fnames = sys.argv[1:]
    for fname in fnames:
        hdr = fits.getheader(fname)
        filter_name = get_filter_name(hdr).replace("'", '').replace(' ', '_')
        print('%s\t%s' % (fname, filter_name))
