#!/usr/bin/env python3
import sys, ccdproc, os, click, subprocess
import astropy.nddata as nddata
import astropy.io.fits as fits
import numpy as np
from astropy.stats import mad_std
import astropy.units as u




def get_filter_name(hdr):
    if 'ALFLTNM' in hdr and hdr['ALFLTNM'] != 'Open':
        return hdr['ALFLTNM']
    if 'FAFLTNM' in hdr and hdr['FAFLTNM'] != 'Open':
        return hdr['FAFLTNM']
    if 'FBFLTNM' in hdr and hdr['FBFLTNM'] != 'Open':
        return hdr['FBFLTNM']
    return ''


COPY_HEADERS=[
    'DATE-OBS',
    'EXPTIME',
    'ALFLTNM',
    'FAFLTNM',
    'FBFLTNM',
    'OBJECT'
]

@click.command()
@click.option('-c', '--calib-dir')
@click.option('-d', '--dest-dir')
@click.argument('raw_frames', nargs=-1)
def main(raw_frames, dest_dir, calib_dir):
    # load master calib frames
    master_bias = nddata.fits_ccddata_reader('%s/master_bias.fits' % calib_dir, unit='adu')
    #master_flat = nddata.fits_ccddata_reader('phot/cals/master_flat.fits', unit='adu')
    #print(master_flat.mask)

    if not os.path.isdir(dest_dir):
        os.makedirs(dest_dir)
    
    # load frames
    for fname in raw_frames:
        hdr = fits.getheader(fname)
        filter_name = get_filter_name(hdr).replace("'", '').replace(' ', '_')
        master_flat = nddata.fits_ccddata_reader('%s/master_flat_%s.fits' % (calib_dir, filter_name), unit='adu')
        
        #print(fname)
        frame = nddata.fits_ccddata_reader(fname, hdu='im1', unit='adu')
        
        # do basic CCD processing
        new_frame = ccdproc.ccd_process(frame, master_bias=master_bias, master_flat=master_flat)
        new_frame.mask = master_flat.mask
        #newnew_frame = nddata.CCDData(data=new_frame.data, mask=mask, wcs=new_frame.wcs, meta=new_frame.meta, unit=new_frame.unit)
        #print(new_frame.mask)
        new_frame.data[np.isinf(new_frame.data)] = np.nan
        #new_frame.mask = None

        new_frame = ccdproc.gain_correct(new_frame, 0.18*u.electron/u.adu)

        #new_frame2 = ccdproc.trim_image(new_frame, "[220:2000,150:1930]")
        new_frame = ccdproc.trim_image(new_frame, "[250:1900,180:1900]")

        new_frame = ccdproc.cosmicray_lacosmic(new_frame, sigclip=7, verbose=True)

        new_frame.data[np.isinf(new_frame.data)] = np.nan
        new_frame.mask = None
        # make sure those keys exist.. so we can replace them later -.-
        new_frame.header['CD1_1'] = 0.0
        new_frame.header['CD1_2'] = 0.0
        new_frame.header['CD2_1'] = 0.0
        new_frame.header['CD2_2'] = 0.0
        for k in COPY_HEADERS:
            new_frame.header[k] = hdr[k]
        dest_fname = os.path.join(dest_dir, os.path.basename(fname))
        new_frame.write(dest_fname, overwrite=True)
        
        # get rid of some junk keywords
        # not sure why the code above doesn't work.. still gets overwritten somehow
        subprocess.check_call("replacekey -p CDELT1 -k D00 %s" % dest_fname, shell=True)
        subprocess.check_call("replacekey -p CDELT2 -k D01 %s" % dest_fname, shell=True)
        subprocess.check_call("replacekey -p PC1_1 -k D11 %s" % dest_fname, shell=True)
        subprocess.check_call("replacekey -p PC1_2 -k D12 %s" % dest_fname, shell=True)
        subprocess.check_call("replacekey -p PC2_1 -k D21 %s" % dest_fname, shell=True)
        subprocess.check_call("replacekey -p PC2_2 -k D22 %s" % dest_fname, shell=True)
        
        # write the initial astrometric solution (scaling matrix)
        subprocess.check_call('replacekey -p CD1_1 -k CD1_1 -v "-5.9571489439E-05" %s' % dest_fname, shell=True)
        subprocess.check_call('replacekey -p CD1_2 -k CD1_2 -v "-2.19801481257E-07" %s' % dest_fname, shell=True)
        subprocess.check_call('replacekey -p CD2_1 -k CD2_1 -v "-2.19801481257E-07" %s' % dest_fname, shell=True)
        subprocess.check_call('replacekey -p CD2_2 -k CD2_2 -v "5.9571489439E-05" %s' % dest_fname, shell=True)
        
    



if __name__ == '__main__':
    main()
