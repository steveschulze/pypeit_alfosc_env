srcdir=$1
day=$2
filtername=$3

# find the correct flat frames
flat_frames=$(dfits $srcdir/*.fits|fitsort imagetyp obs_mode object FAFLTNM DETWIN1|grep "FLAT,SKY"|grep "$filtername"|grep "[1:2148, 1:2102]"|cut -f1)

#dfits $srcdir/*.fits|fitsort imagetyp obs_mode object FAFLTNM DETWIN1|grep "FLAT,SKY"|grep "$filtername"|grep "[1:2148, 1:2102]"

# do the ccd reduction
# for in_frame in $flat_frames; do
#     ficalib --mosaic "size=(2148,2102),name=im1" -B photcals/master_bias.fits --post-scale 1 -i $in_frame -o tmp/$(basename $in_frame)
# done
python3 scripts/phot_make_master_flat.py ${flat_frames}
