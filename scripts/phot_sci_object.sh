#!/bin/sh
day=$1
object=$2

raw_frames=$(dfits raw/$day/*.fits|fitsort obs_mode object|grep IMAGING|grep $object|cut -f1)
detrend_dir=phot/detrend/$object/$day
pipenv run python3 scripts/phot_detrend_sci_frames.py -d $detrend_dir -c phot/calibs/$day $raw_frames

# do astrometry for all filters
gp3-astrometry --contrast-threshold 1.0 phot/detrend/$object/$day/*.fits

mkdir -p phot/stacked/$object

# used filters
filters=$(pipenv run python3 scripts/phot_extract_filtername.py $detrend_dir/*.fits|cut -f2|sort|uniq)

for filter in $filters; do
    fnames=$(pipenv run python3 scripts/phot_extract_filtername.py $detrend_dir/*.fits|grep $filter|cut -f1)
    SWarp -IMAGEOUT_NAME phot/stacked/$object/${day}_${filter}.fits $fnames
done

