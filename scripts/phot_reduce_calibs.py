#!/usr/bin/env python
import click, ccdproc
import numpy as np




ALFOSC_KEYWORDS = [
    'IMAGETYP',
    'IMAGECAT',
    'OBS_MODE',
    'OBJECT',
    'FAFLTNM', # photometric filter
    'DETWIN1'
]


@click.command()
@click.option('--destdir')
@click.option('--detwin1', default='[1:2148, 1:2102]')
@click.argument('calibfiles', nargs=-1)
def main(calibfiles, destdir, detwin1):
    # load headers of calib frames
    imgs = ccdproc.ImageFileCollection(filenames=calibfiles, keywords=ALFOSC_KEYWORDS)

    # find the correct bias frames
    print('* Creating master bias..')
    bias_frames = (imgs.summary['IMAGETYP'] == 'BIAS') & (imgs.summary['OBS_MODE'] == 'IMAGING') & (imgs.summary['DETWIN1'] == detwin1)
    print(bias_frames)
    #bias_frames = list(imgs.ccds(IMAGETYP='BIAS', OBS_MODE='IMAGING', DETWIN1=detwin1))
    print('\tFound %d suitable bias frames' % len(bias_frames))
    print(bias_frames[0])
    bias_combiner = ccdproc.Combiner(bias_frames)
    bias_combiner.clip_extrema(nlow=1, nhigh=2)
    master_bias = bias_combiner.average_combine()
    
    



if __name__ == '__main__':
    main()
