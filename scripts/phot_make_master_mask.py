import sys,ccdproc
import astropy.nddata as nddata
import astropy.io.fits as fits
import numpy as np




if __name__ == '__main__':
    img1 = nddata.fits_ccddata_reader(sys.argv[1], hdu='im1', unit='adu')
    img2 = nddata.fits_ccddata_reader(sys.argv[2], hdu='im1', unit='adu')
    ratio = img1.divide(img2)
    mask = ccdproc.ccdmask(ratio) | img1.data < 1000
    print(mask)
    new_data = img1.data
    new_data[mask] = 0
    new_data[~mask] = 1
    mask_frame = nddata.CCDData(data=new_data, mask=mask, unit='adu')
    mask_frame.write('photcals/master_mask.fits')
