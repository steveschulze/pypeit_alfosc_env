input_frames=$@

pipenv run python3 scripts/phot_detrend_sci_frames.py $input_frames

for inframe in $input_frames; do
    new_fname=phot/detrend/$(basename $inframe)
    replacekey -p CDELT1 -k D00  $new_fname
    replacekey -p CDELT2 -k D01  $new_fname
    replacekey -p PC1_1 -k D11 $new_fname
    replacekey -p PC1_2 -k D12 $new_fname
    replacekey -p PC2_1 -k D21 $new_fname
    replacekey -p PC2_2 -k D22 $new_fname
    replacekey -p CD1_1 -k CD1_1 -v "-5.9571489439E-05" $new_fname
    replacekey -p CD1_2 -k CD1_2 -v "-2.19801481257E-07" $new_fname
    replacekey -p CD2_1 -k CD2_1 -v "-2.19801481257E-07" $new_fname
    replacekey -p CD2_2 -k CD2_2 -v "5.9571489439E-05" $new_fname
done
